package game;

public class Person {
    private int x;
    private int y;
    private char portrait;
    private PersonType personType;

    public Person(int x, int y, char portrait, PersonType pt) {
        this.x = x;
        this.y = y;
        this.portrait = portrait;
        this.personType = pt;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int value) {
        this.x = value;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int value) {
        this.y = value;
    }

    public char getPortrait() {
        return this.portrait;
    }

    public PersonType getType() {
        return this.personType;
    }
}
