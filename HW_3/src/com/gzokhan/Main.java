package com.gzokhan;

import game.GameEngine;
import game.GameState;

public class Main {

    public static void main(String[] args) {
        GameEngine ge = new GameEngine();
        ge.generateLevel();
        GameState gs = GameState.InProcess;
        ge.draw();
        while (gs == GameState.InProcess) {
            gs = ge.turn();
        }
        if (gs == GameState.Defeat) {
            System.out.println("Вас съело чудовище!");
        }
        if (gs == GameState.Victory) {
            System.out.println("Вы нашли ее!");
        }
    }
}
