package bank;

public class DebitCard extends Card {

    public DebitCard(String userName) {
        super(userName);
    }

    public DebitCard(String userName, double balance) {
        super(userName, balance);
    }

    public @Override void reduceBalance(double value){
        double result = balance - value;
        if (result > 0){
            balance = result;
        }
    }
}
