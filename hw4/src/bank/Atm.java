package bank;

public class Atm {

    public Atm(){

    }

    public double getBalance(Card card){
        return card.getBalance();
    }

    public void addBalance(Card card, double value){
        card.addBalance(value);
    }

    public void reduceBalance(Card card, double value){
        card.reduceBalance(value);
    }
}
