package bank;

import org.junit.Assert;

import static org.junit.Assert.*;

public class AtmTest {

    @org.junit.Test
    public void addManyBalance() {
        Atm atm = new Atm();
        Card c = new Card("Adun");
        atm.addBalance(c, 1000000);
        Assert.assertEquals(c.getBalance(), 1000000, 0.001);
    }

    @org.junit.Test
    public void addZero() {
        Atm atm = new Atm();
        Card c = new Card("Adun");
        atm.addBalance(c, 0);
        Assert.assertEquals(c.getBalance(), 0, 0.001);
    }

    @org.junit.Test
    public void add10() {
        Atm atm = new Atm();
        Card c = new Card("Adun");
        atm.addBalance(c, 10);
        Assert.assertEquals(c.getBalance(), 10, 0.001);
    }

    @org.junit.Test
    public void reduce20FromDebit10() {
        Atm atm = new Atm();
        DebitCard dc = new DebitCard("Adun", 10);
        atm.reduceBalance(dc, 20);
        Assert.assertEquals(dc.getBalance(), 10, 0.001);
    }

    @org.junit.Test
    public void reduce5FromDebit10() {
        Atm atm = new Atm();
        DebitCard dc = new DebitCard("Adun", 10);
        atm.reduceBalance(dc, 5);
        Assert.assertEquals(dc.getBalance(), 5, 0.001);
    }
}