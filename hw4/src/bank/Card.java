package bank;

public class Card {

    protected String userName;
    protected double balance;

    public Card(String userName) {
        this.userName = userName;
        balance = 0;
    }

    public Card(String userName, double balance) {
        this.userName = userName;
        this.balance = balance;
    }

    public void addBalance(double value){
        this.balance += value;
    }

    public void reduceBalance(double value){
        this.balance -= value;
    }

    public String getUserName(){
        return this.userName;
    }

    public double getBalance(){
        return this.balance;
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public void setBalance(double balance){
        this.balance = balance;
    }

}
