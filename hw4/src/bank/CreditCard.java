package bank;

public class CreditCard extends Card {

    public CreditCard(String userName) {
        super(userName);
    }

    public CreditCard(String userName, double balance) {
        super(userName, balance);
    }
}
