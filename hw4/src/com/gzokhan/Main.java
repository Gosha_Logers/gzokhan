package com.gzokhan;
import bank.*;

public class Main {

    public static void main(String[] args) {
        AtmTest at = new AtmTest();
        at.add10();
        at.addManyBalance();
        at.addZero();
        at.reduce5FromDebit10();
        at.reduce20FromDebit10();
    }
}
