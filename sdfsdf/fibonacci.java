//package com.gzokhan.fibonacci;

public class fibonacci 
{
public static void main(String[] args) 
{
int algorithmld, loopType, n; 
try
{
algorithmld = Integer.parseInt(args[0]);
loopType = Integer.parseInt(args[1]);
n = Integer.parseInt(args[2]);

}
catch(IllegalArgumentException e)
{
 System.out.println("Incorrect data");
            throw e;
}
catch(ArrayIndexOutOfBoundsException e)
{
System.out.println("Not enough args");
            throw e;
}
 Calc(algorithmld, loopType, n);
 System.out.print("");
 Result();
}
public static double Calc(int algorithmld, int loopType, int n)
{

if(algorithmld < 0 || algorithmld > 2)
{
System.out.print("Error:Incorrect Data");
return 0;
}

if(loopType< 0 || loopType > 3)
{
System.out.print("Error:Incorrect Data");
return 0;
}

if(n < 0)
{
System.out.print("Error:Incorrect Data");
return 0;
}

if(algorithmld == 1)
{

int n0 = 1;
int n1 = 1;
int n2;

if(n == 0)
{
System.out.print("0");
return 0;
}
if(n == 1)
{
System.out.print("0 1");
return 0;
}
if(loopType == 1)
{
int i = 3;
System.out.print("0 "+ n0+" "+n1+" ");
while(i <= n){
n2=n0+n1;
System.out.print(n2+" ");
n0=n1;
n1=n2;
i++;
		}
return 0;
}
if(loopType == 2)
{
int i = 3;
System.out.print("0 "+ n0+" "+n1+" ");
do{
n2=n0+n1;
System.out.print(n2+" ");
n0=n1;
n1=n2;
i++;
}while(i <= n);
return 0;
}
if(loopType == 3)
{
System.out.print("0 "+ n0+" "+n1+" ");
for(int i = 3; i <= n; i++){
n2=n0+n1;
System.out.print(n2+" ");
n0=n1;
n1=n2;
		}
return 0;
}
}


if(algorithmld == 2)
{
long ret = 1;
if(loopType == 1)
{
 int i = 1;
while(i <= n)
{
ret *= i;
i++;
}
System.out.print(ret);
return ret;
}
if(loopType == 2)
{
int i = 1;
do{
ret *= i;
i++;
}while(i <= n);
System.out.print(ret);
return ret;
}
if(loopType == 3)
{
for (int i = 1; i <= n; ++i) ret *= i;
System.out.print(ret);
return ret;
}

}

return 1;
}
public static void Result()
{
  double x = Calc(2 ,1, 3);
  if(x == 6)
{
 System.out.print("True");
}
else
{
System.out.print("False");
}

}
}